package dto

import "github.com/google/uuid"

type UserUpdateDto struct {
	ID       uuid.UUID `json:"id" form:"id" binding:"required"`
	Name     string    `json:"name" form:"name" binding:"required"`
	Email    string    `json:"email" form:"email" binding:"required" validate:"email"`
	Password string    `json:"password,omitempty" form:"password,omitempty" validate:"min:6"`
}

type UserCreateDto struct {
	Name     string `json:"name" form:"name" binding:"required"`
	Email    string `json:"email" form:"email" binding:"required" validate:"email"`
	Password string `json:"password,omitempty" form:"password,omitempty" validate:"min:6"`
}
