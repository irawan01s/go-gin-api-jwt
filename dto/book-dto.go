package dto

import "github.com/google/uuid"

type BookUpdateDto struct {
	ID          uuid.UUID `json:"id" form:"id" binding:"required"`
	Title       string    `json:"title" form:"title" binding:"required"`
	Description string    `json:"description" form:"description" binding:"required"`
	UserID      uuid.UUID `json:"user_id,omitempty" form:"user_id,omitempty" binding:"required"`
}

type BookCreateDto struct {
	Title       string    `json:"title" form:"title" binding:"required"`
	Description string    `json:"description" form:"description" binding:"required"`
	UserID      uuid.UUID `json:"user_id,omitempty" form:"user_id,omitempty" binding:"required"`
}
