package main

import (
	"example/go-gin-api/config"
	"example/go-gin-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db          *gorm.DB            = config.ConnectDB()
	authHandler handler.AuthHandler = handler.NewAuthHandler()
)

func main() {
	defer config.CloseConnectDB(db)
	r := gin.Default()

	authRoutes := r.Group("/api/auth")
	{
		authRoutes.POST("/login", authHandler.Login)
		authRoutes.POST("/register", authHandler.Register)
	}

	r.Run()
}
