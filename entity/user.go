package entity

import "github.com/google/uuid"

type User struct {
	ID       uuid.UUID `gorm:"primaryKey" json:"id"`
	Name     string    `gorm:"type:varchar(255)" json:"name"`
	Email    string    `gorm:"uniqueIndex;type:varchar(255)" json:"email"`
	Password string    `gorm:"->:<-not null" json:"password"`
	Token    string    `gorm:"-" json:"token,omitempty"`
}
